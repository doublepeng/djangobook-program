# Create your views here.
from django.http import *
from django.shortcuts import render_to_response
from mysite.books.models import Book
from django.core.mail import send_mail

def search_form(request):
    return render_to_response('search_form.html')
def search_old(request):
    if request.GET['q']:
        message = 'you searched for: %r'%request.GET['q']
    else:
        message = 'you submitted an empty form'
    return HttpResponse(message)

def search_old2(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        books = Book.objects.filter(title__icontains=q)
        return render_to_response('search_results.html',locals())
    else:
        #return render_to_response('please submit a search term')
        return render_to_response('search_form.html',{'error':True})

def search(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            books = Book.objects.filter(title__icontains = q)
            return render_to_response('search_results.html',locals())
    return render_to_response('search_form.html',{'error':error})


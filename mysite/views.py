# -*- coding utf-8 -*-
from django.http import *
from django.template.loader import get_template
import datetime
from django.template import Template,Context
from django.shortcuts import render_to_response
def hello(request):
    now = datetime.datetime.now()
    html = "<html><body>hello pengchao.It is now %s.</body></html>" %now
    return HttpResponse(html)

def requestinfo(request):
    ua = request.META['HTTP_USER_AGENT']
    t = Template("""<html><body>{{ua}}<body><html>""")
    c = Context({'ua':ua})
    html =  t.render(c)
    return HttpResponse(html)

def ua_display(request):
    values = request.META.items()
    values.sort()
    return render_to_response('ua_display.html',locals())


def hours_ahead(request,offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now()+datetime.timedelta(hours=offset)
    html = "<html><body>In %s hours,it will be %s. </body></html>"%(offset,dt)
    return HttpResponse(html)

class Person(object):
    def __init__(self,first_name,last_name):
        self.first_name,self.last_name = first_name,last_name


def current_datetime(request):
    current_date= datetime.datetime.now()
    #t = Template("<html><body>it is now {{current_date}} </body></html>")
    #t = get_template('current_datetime.html')
    #html = t.render(Context({'current_date':now}))
    #return HttpResponse(html)
    return render_to_response('current_datetime.html',locals())

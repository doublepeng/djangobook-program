from django.conf.urls.defaults import *
#from mysite.views import hello,requestinfo,hours_ahead,current_datetime,ua_display
from mysite.views import *
from mysite.books import views
from django.contrib import admin
from mysite.contact import views as contact
admin.autodiscover()
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    (r'^$',hello),
    (r'^hello/$',hello),
    (r'^hours/(\d{1,2})/$',hours_ahead),
    (r'^current_datetime/$',current_datetime),
    (r'^requestinfo/$',requestinfo),
    (r'^ua_display/$',ua_display),
    (r'^search_form/$',views.search_form),
    (r'^search/$',views.search),
    (r'^contact/thanks/$',contact.thanks),
    (r'^contact/$',contact.contact),
    # Example:
    # (r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
)
